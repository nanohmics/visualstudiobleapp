NOTE: The code is in the feature branch.

Status: 
This was built upon Microsoft’s example BLE application, which has three scenarios the user can click through. 
Scenario 1 is mandatory- it scans for available devices and allows pairing. Then the user can click between 
Scenarios 2 and 3 as desired. Scenario 2 displays peripheral device’s full GATT, with periodic updates (the user 
can also manually refresh the table), and allows for read/write/notify capabilities. Scenario 3 allows the user 
to select a specific characteristic and plot the value over time. Both Scenarios 2 and 3 allow the user to select 
the location and name of the data log (note if the file name is the same as an existing file, it will append not 
overwrite).

NEXT STEPS:
•	This application is pretty much ready to go for general debugging purposes.
•	Suggested improvements:
	•	Verify Scenario 3 can handle plotting non-binary values over time (i.e. can convert hex to decimal). 
		Right now misreads or incompatible formats are plotted as -1 value.
	•	Package so non-developer users can run.

How to Run Application as a Developer:
•	Install Visual Studio
	•	https://visualstudio.microsoft.com/downloads/ 
•	Clone Bitbucket repository: VisualStudioBLEApp
	•	Note that the code is in the feature branch
	•	Navigate: feature branch -> BluetoothLE -> cs -> BluetoothLE.sln
•	If experience difficulties building, check the installed packages
	•	Right click on the Project Name (BluetoothLE) in the Solution Explorer
	•	Select Manage NuGet Packages
	•	If needed, install WinRTXamlToolkit.Controls.DataVisualization.UWP